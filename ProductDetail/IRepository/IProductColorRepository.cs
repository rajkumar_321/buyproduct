﻿using ProductDetail.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductDetail.IRepository
{
    public interface IProductColorRepository
    {

        Task<int> Create(ProductColor products);
        Task<List<ProductColor>> GetAll();
        IEnumerable<ProductColor> GetByIdAsync(int id);
        Task<string> Update(int id, ProductColor products);
        Task<string> Delete(int id);

    }
}
