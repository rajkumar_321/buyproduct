﻿using Microsoft.AspNetCore.Mvc;
using ProductDetail.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductDetail.IRepository
{
    public interface IProductRepository
    {
            Task<int> Create(Product products);
            Task<object> GetAll();
            IEnumerable<Product> GetByIdAsync(int id);
            Task<string> Update(int id, Product products);
            Task<string> Delete(int id);
        
    }
}
