﻿using ProductDetail.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductDetail.IRepository
{
    public interface IProductSizeRepository
    {
        Task<int> Create(ProductSize products);
        Task<List<ProductSize>> GetAll();
        IEnumerable<ProductSize> GetByIdAsync(int id);
        Task<string> Update(int id, ProductSize products);
        Task<string> Delete(int id);
    }
}
