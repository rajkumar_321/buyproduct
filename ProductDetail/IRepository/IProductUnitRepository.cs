﻿using ProductDetail.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductDetail.IRepository
{
    public interface IProductUnitRepository
    {

        Task<int> Create(ProductUnit products);
        Task<List<ProductUnit>> GetAll();
        IEnumerable<ProductUnit> GetByIdAsync(int id);
        Task<string> Update(int id, ProductUnit products);
        Task<string> Delete(int id);

    }
}
