﻿
using Microsoft.AspNetCore.Mvc;
using ProductDetail.Entities;
using ProductDetail.IRepository;
using ProductDetail.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductDetail
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductRepository _productRepos;
        public ProductController(IProductRepository productRepos)
        {
            _productRepos = productRepos;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> create([FromBody] Product product)
        {
            int productId = await _productRepos.Create(product);
            return Ok(productId);
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult> GetAll()
        {
            var productDetails = await _productRepos.GetAll();
            return Ok(productDetails);
        }

        [HttpGet]
        [Route("GetItemAll")]
        public async Task<object> GetItemall()
        {
            var productDetails = await _productRepos.GetAll();
            return Ok(productDetails);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public IEnumerable<Product> GetById(int id)
        {
            var product =  _productRepos.GetByIdAsync(id);
            return product;
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> Update(int id, Product product)
        {
            string resp = await _productRepos.Update(id, product);
            return Ok(resp);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var resp = await _productRepos.Delete(id);
            return Ok(resp);
        }
    }
}
