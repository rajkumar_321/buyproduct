﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductDetail.Entities;
using ProductDetail.IRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductDetail.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductUnitController : ControllerBase
    {
        private IProductUnitRepository _productRepos;
        public ProductUnitController(IProductUnitRepository productRepos)
        {
            _productRepos = productRepos;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> create([FromBody] ProductUnit product)
        {
            int productId = await _productRepos.Create(product);
            return Ok(productId);
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult> GetAll()
        {
            var productDetails = await _productRepos.GetAll();
            return Ok(productDetails);
        }

        [HttpGet]
        [Route("GetItemAll")]
        public async Task<object> GetItemall()
        {
            var productDetails = await _productRepos.GetAll();
            return Ok(productDetails);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public IEnumerable<ProductUnit> GetById(int id)
        {
            var product = _productRepos.GetByIdAsync(id);
            return product;
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IActionResult> Update(int id, ProductUnit product)
        {
            string resp = await _productRepos.Update(id, product);
            return Ok(resp);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var resp = await _productRepos.Delete(id);
            return Ok(resp);
        }
    }
}
