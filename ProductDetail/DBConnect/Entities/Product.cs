﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductDetail.Entities
{
    public class ProductUnit
    {
        public int ProductUnitID { get; set; }
        public string ProductUnitName { get; set; }
        public string ProductUnitDescription { get; set; }
        public List<Product> Products { get; set; }
    }

    public class ProductSize
    {
        public int ProductSizeID { get; set; }
        public string ProductSizeName { get; set; }
        public string ProductSizeDescription { get; set; }
        public List<Product> Products { get; set; }
    }

    public class ProductColor
    {
        public int ProductcolorID { get; set; }
        public string ProductcolorName { get; set; }
        public string ProductcolorDescription { get; set; }
        public List<Product> Products { get; set; }
    }

    public class Product
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }
        [Required]
        [ForeignKey("FK_ProductUnit")]
        public int ProductUnitId { get; set; }
        [Required]
        [ForeignKey("FK_ProductColor")]
        public int ProductColorId { get; set; }
        [Required]
        [ForeignKey("FK_ProductSize")]
        public int ProductSizeId { get; set; }

        [Required]
        [MaxLength(50)]
        public string ProductName { get; set; }

        [Required]
        [MaxLength(1000)]
        public string ProductDescription { get; set; }

        

    }

}
