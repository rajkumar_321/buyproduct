﻿using Microsoft.EntityFrameworkCore;
using ProductDetail.Entities;

namespace ProductDetail.DBConnect
{
    public class RepositoryDbContext : DbContext
    {
        public RepositoryDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductUnit> Productunits { get; set; }
        public DbSet<ProductSize> Productsizes { get; set; }
        public DbSet<ProductColor> Productcolors { get; set; }



    }
}
