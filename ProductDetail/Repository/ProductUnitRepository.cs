﻿using Microsoft.EntityFrameworkCore;
using ProductDetail.DBConnect;
using ProductDetail.Entities;
using ProductDetail.IRepository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductDetail.Repository
{
    public class ProductUnitRepository : IProductUnitRepository
    {
        private RepositoryDbContext _dbcontext;
        public ProductUnitRepository(RepositoryDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }
        public async Task<int> Create(ProductUnit products)
        {
            _dbcontext.Productunits.Add(products);
            await _dbcontext.SaveChangesAsync();
            //var starTrek = new ProductColor
            //{
            //    ProductcolorName = products.ProductcolorName,
            //    ProductcolorDescription = products.ProductcolorDescription
            //};

            return products.ProductUnitID;
        }

        public async Task<string> Delete(int id)
        {
            var productUnitDel = _dbcontext.Productunits.Where(produnitid => produnitid.ProductUnitID == id).FirstOrDefault();
            if (productUnitDel == null) return "Product Unit does not exists";
            _dbcontext.Productunits.Remove(productUnitDel);
            await _dbcontext.SaveChangesAsync();
            return "Product Unit details deleted modified";
        }

        public async Task<List<ProductUnit>> GetAll()
        {
            var products = await _dbcontext.Productunits.ToListAsync<ProductUnit>();
            return products;
        }

        public IEnumerable<ProductUnit> GetByIdAsync(int id)
        {
            var product = _dbcontext.Productunits.Where(prountid => prountid.ProductUnitID == id).ToList();

            return product;
        }

        public async Task<string> Update(int id, ProductUnit products)
        {
            var productSizeupdate = await _dbcontext.Productunits.Where(product => product.ProductUnitID == id).FirstOrDefaultAsync();
            if (products == null) return "Product does not exists";
            productSizeupdate.ProductUnitName = products.ProductUnitName;
            productSizeupdate.ProductUnitDescription = products.ProductUnitDescription;

            await _dbcontext.SaveChangesAsync();
            return "Product details successfully modified";
        }
    }
}
