﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProductDetail.DBConnect;
using ProductDetail.Entities;
using ProductDetail.IRepository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductDetail.Repository
{
    public class ProductRepository : IProductRepository
    {
        private RepositoryDbContext _dbcontext;
        public ProductRepository(RepositoryDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }
        public async Task<int> Create(Product product)
        {
            _dbcontext.Products.Add(product);
            await _dbcontext.SaveChangesAsync();
            return product.ProductId;
        }
        public async Task<List<Product>> GetAll()
        {
            var products = await _dbcontext.Products.ToListAsync<Product>();

           
            return products;
        }
        public IEnumerable<Product> GetByIdAsync(int id)
        {
            var product = _dbcontext.Products.Where(proid => proid.ProductId == id).ToList();

            return product;
        }
        public async Task<string> Update(int id, Product product)
        {
            var productupdate = await _dbcontext.Products.Where(product => product.ProductId == id).FirstOrDefaultAsync();
            if (product == null) return "Product does not exists";
            productupdate.ProductName = product.ProductName;
            productupdate.ProductDescription = product.ProductDescription;
            
            await _dbcontext.SaveChangesAsync();
            return "Product details successfully modified";
        }
        public async Task<string> Delete(int id)
        {
            var productDel = _dbcontext.Products.Where(proid => proid.ProductId == id).FirstOrDefault();
            if (productDel == null) return "Product does not exists";
            _dbcontext.Products.Remove(productDel);
            await _dbcontext.SaveChangesAsync();
            return "Product details deleted modified";
        }

        async Task<object> IProductRepository.GetAll()
        {
            var test1 = await(from c in _dbcontext.Products
                              join e in _dbcontext.Productunits
                                  on c.ProductUnitId equals e.ProductUnitID
                              join t in _dbcontext.Productcolors
                              on c.ProductColorId equals t.ProductcolorID
                              join p in _dbcontext.Productsizes on c.ProductSizeId equals p.ProductSizeID
                              select new
                              {
                                  id = c.ProductId,
                                  productNam = c.ProductName,
                                  productDesc = c.ProductDescription,
                                  productcolor = t.ProductcolorName,
                                  productSize = p.ProductSizeName,
                                  productunit = e.ProductUnitName

                              }).ToListAsync();

            var json = JsonConvert.SerializeObject(test1, Formatting.Indented,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                }
            );
            return json;
        }
    }
}
