﻿using Microsoft.EntityFrameworkCore;
using ProductDetail.DBConnect;
using ProductDetail.Entities;
using ProductDetail.IRepository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductDetail.Repository
{
    public class ProductColorRepository : IProductColorRepository
    {
        private RepositoryDbContext _dbcontext;
        public ProductColorRepository(RepositoryDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }
        public async Task<int> Create(ProductColor products)
        {
            _dbcontext.Productcolors.Add(products);
            await _dbcontext.SaveChangesAsync();
            //var starTrek = new ProductColor
            //{
            //    ProductcolorName = products.ProductcolorName,
            //    ProductcolorDescription = products.ProductcolorDescription
            //};

            return products.ProductcolorID;
        }

        public async Task<string> Delete(int id)
        {
            var productColorDel = _dbcontext.Productcolors.Where(prodcolorid => prodcolorid.ProductcolorID == id).FirstOrDefault();
            if (productColorDel == null) return "Product color does not exists";
            _dbcontext.Productcolors.Remove(productColorDel);
            await _dbcontext.SaveChangesAsync();
            return "Product color details deleted modified";
        }

        public async Task<List<ProductColor>> GetAll()
        {
            var products = await _dbcontext.Productcolors.ToListAsync<ProductColor>();
            return products;
        }

        public IEnumerable<ProductColor> GetByIdAsync(int id)
        {
            var product = _dbcontext.Productcolors.Where(procolorid => procolorid.ProductcolorID == id).ToList();

            return product;
        }

        public async Task<string> Update(int id, ProductColor products)
        {
            var productcolorupdate = await _dbcontext.Productcolors.Where(product => product.ProductcolorID == id).FirstOrDefaultAsync();
            if (products == null) return "Product does not exists";
            productcolorupdate.ProductcolorName = products.ProductcolorName;
            productcolorupdate.ProductcolorDescription = products.ProductcolorDescription;

            await _dbcontext.SaveChangesAsync();
            return "Product details successfully modified";
        }
    }
}
