﻿using Microsoft.EntityFrameworkCore;
using ProductDetail.DBConnect;
using ProductDetail.Entities;
using ProductDetail.IRepository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductDetail.Repository
{
    public class ProductSizeRepository : IProductSizeRepository
    {
        private RepositoryDbContext _dbcontext;
        public ProductSizeRepository(RepositoryDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }
        public async Task<int> Create(ProductSize products)
        {
            _dbcontext.Productsizes.Add(products);
            await _dbcontext.SaveChangesAsync();
            //var starTrek = new ProductColor
            //{
            //    ProductcolorName = products.ProductcolorName,
            //    ProductcolorDescription = products.ProductcolorDescription
            //};

            return products.ProductSizeID;
        }

        public async Task<string> Delete(int id)
        {
            var productSizeDel = _dbcontext.Productsizes.Where(prodsizeid => prodsizeid.ProductSizeID == id).FirstOrDefault();
            if (productSizeDel == null) return "Product Size does not exists";
            _dbcontext.Productsizes.Remove(productSizeDel);
            await _dbcontext.SaveChangesAsync();
            return "Product Size details deleted modified";
        }

        public async Task<List<ProductSize>> GetAll()
        {
            var products = await _dbcontext.Productsizes.ToListAsync<ProductSize>();
            return products;
        }

        public IEnumerable<ProductSize> GetByIdAsync(int id)
        {
            var product = _dbcontext.Productsizes.Where(prosizeid => prosizeid.ProductSizeID == id).ToList();

            return product;
        }

        public async Task<string> Update(int id, ProductSize products)
        {
            var productSizeupdate = await _dbcontext.Productsizes.Where(product => product.ProductSizeID == id).FirstOrDefaultAsync();
            if (products == null) return "Product does not exists";
            productSizeupdate.ProductSizeName = products.ProductSizeName;
            productSizeupdate.ProductSizeDescription = products.ProductSizeDescription;

            await _dbcontext.SaveChangesAsync();
            return "Product details successfully modified";
        }
    }
}
